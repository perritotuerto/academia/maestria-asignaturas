# Mercancía, capital y cinematografía

* Entidad: 10
* Asignatura: 63044
* Grupo: T112
* Créditos: 8
* Tipo: Seminario
* Campo: Filosofía de la cultura
* Profesor(es): Dr. Carlos Oliva Mendoza

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63044_t112_seminario_mercancia-capital-y-cinematog](https://pad.kefir.red/p/63044_t112_seminario_mercancia-capital-y-cinematog).
