# *El capital*, libro 1, vol. 1, Karl Marx

## Capítulo I. La mercancía

### 1. Los dos factores de la mercancía

Capitalismo = enorme cúmulo de mercancías.
  - Mercancía individual como forma elemental.
=> Análisis, punto de partida.

Mercancía: una cosa que satisface necesidades humanas.
  - No importa cómo, si como subsistencia o producción (directamente o rodeo).
  - Considerarse desde dos enfoques:
    1. Cualidad.
    2. Cantidad.
=> Tiene muchas propiedades > útil en diversos aspectos > múltiples usos >
   constitución de un hecho histórico.
  - Medidas sociales para indicarla cantidad de cosas útiles.

La utilidad genera valor de uso.
  - Condicionada al cuerpo de la mercancía.
  - Lo hace ser un bien.
  - No depende de la cantidad de trabajo necesario para obtenerlo.
  - Supone una determinación cuantitativa.
  - Se efectiviza en eluso o en el consumo.
  - Constituye el contenido material de la riqueza.
  - En la forma social tienen un valor de cambio.

El valor de cambio se presenta como relación cuantitativa.
  - Intercambio de valores de usode clases distintas.
  - Implica una relación que se modifica según el tiempo y el lugar.
  - Es contingente y relativo.

La equivalencia en el valor de cambio demuestra que existe algo en común entre
las mercancías.
  - Dos mercancías en comparación son iguales a una tercera reducible.
  - No es propiedad natural.
    - Solo consideradas en lo que las hace útiles = en su valor de uso.
  - Es una abstracción del valor de uso.

Valor de uso ≠ valor de cambio.
  - El valor de uso es diferente según la cualidad de la mercancía.
  - El valor de cambio es diferente según la cantidad de la mercancía.

Si se pone de lado el valor de uso solo queda la propiedad de ser un producto
del trabajo.
  - Esto también implica que:
    - Se desvanece las formas de trabajo concreto.
    - Se reduce a trabajo humano diferenciado, a trabajo abstractamente humano.
    - Solo queda la objetividad *espectral*.
    - Ya no se considera cómo se gastó la fuerza de trabajo.
=> Se obtiene su valor.
  - Ese algo común que semanifiesta en el valor de cambio.
  => Un bien solo tiene valor cuando se a objetivizado o materializado el
     trabajo abstractamente humano.
    - La magnitud del valor es relativa a la cantidad de trabajo empleado.

La cantidad de trabajo abstracto es:
  - El tiempo de trabajo promedio.
  - El tiempo de trabajo socialmente necesario.
  - El tiempo requerido para producir un valor de uso cualquiera en las
    condiciones de producción vigentes y con el grado de destreza medio.
=> Determinan su magnitud de valor.
  - Permite equiparación.

La magnitud de valor varía según la fuerza productiva de trabajo.
  - La fuerza se determina por:
    - Nivel medio de destreza.
    - Estadio de desarrollo de la ciencia.
    - Coordinación social del proceso de producción.
    - Escala y eficacia de los medios de producción.
    - Las condiciones naturales.
  => +fuerza = -tiempo de producción = -valor.
=> La magnitud de valor es inversamente proporcional a la fuerza productiva
de trabajo.

Clarificaciones:
  - Una cosa puede ser valor de uso y no ser valor.
    - Cuando la utilidad no ha sido medida por el trabajo.
  - Una cosa puede ser medida por el trabajo y no ser mercancía.
    - Cuando el producto satisface una necesidad propia.
  - Una mercancía implica producir valores de usos para otros.
  - Una cosa no puede ser valor si no es un objeto para el uso.

### 2. Dualidad de trabajo representado en las mercancías

El valor de uso y de cambio implica un carácter del trabajo contenido en la
mercancía.

El trabajo útil es el que se representa en el valor de uso.
  - Implica una diferencia cualitativa que permite la comparación entre
    mercancías.
  - Implica una división social del trabajo.
    - Condición para la producción de mercancías, aunque no es una condición
      recíproca.
=> Solo los trabajos privados, independientes y autónomos se enfrentan como
   mercancías.
  - Esto se desenvuelve en un sistema de división social del trabajo.
  - Media la vida humana.

Los valores de uso son una combinación entre:
  - Materia natual.
  - Trabajo.
=> El trabajo no es la única fuente de los valores de uso.

El trabajo medio simple varíasegún el país o la época.
  - El trabajo más complejo solo es trabajo multiplicado.
    - Permite la abstracción que posibilita la equiparación entre cualidades
      y cantidades distintas.
    - Permite que las mercancías en cierta proporción sean valores iguales.

Una cantidad mayor de valor de uso constituye una cantidad de riqueza mayor.
  - Pero esta riqueza provoca una disminución en la magnitud de valor.
=> El aumento o reducción de la fuerza productiva implica que el trabajo útil
   es más abundante o exigua.
  - Carácter dual del trabajo.

El trabajo, aunque cambie de fuerza productiva, tiene la misma magnitud de
valor en *los mismos espacios de tiempo*.
  - Pero da valores de uso en diferentes cantidades, según la fuerza productiva.
=> En la suma de tiempos se reduce la magnitud de valor al aumentar el valor
   de uso.

El trabajo humano es:
  - Fuerza fisiológica humana.
  - Productora de valores de uso.

### 3. La forma de valor o el valor de cambio

Las mercancías tienen forma doble:
  - Natural.
  - De valor.
=> Su objetividad es solo social.
  - Dependiente de la relación social entre diferentes mercancías.

Retorno a la forma como se manifiesta el valor.
  - Dilucidar la génesis de la forma dineraria mediante el desarrollo de
    la expresión de valor.
  - Desvanecer el enigma del dinero.

#### A. Forma simple o singular del valor

Ax = By

Toda forma de valor depende de esta forma simple de valor.

Polos de la expresión de valor:
  - La forma relativa de valor.
  - La forma de equivalente.

La forma relativa es cuando el valor solo puede expresarse mediante otra
mercancía.
  - Supone un equivalente.
=> Puede presentarse en ambas formas, pero no simultáneamente.

Supone que apesar de ser diferentes cosas, son cosas de igual naturaleza.
  - Implica la abstracción del valor.

El trabajo humano crea valor pero no es valor.
  - Se convierte en valor al objetivarse, al abstraerse.

La forma de valor no solo expresa un valor en general sino también como
una magnitud cuantificada de valor.
  - La equiparación supone un tiempo de trabajo igual.
    - Implica que son comparadas con las horas de trabajo.

Las horas de trabajo son distintas al valor que revisten.
  - No hay misterio, es trabajo humano en general.

La forma equivalente:
  1. El valor de uso se convierte en valor.
  2. El trabajo concreto se convierte en abstracto.
  3. El trabajo privado se convierte en social.

Con precisión la mercancía como valor de uso y de cambio es una falsa
apreciación.
  - Es una antítesis interna y externa: los valores no se muestran
    *simultáneamente* en sí o entre mercancías.

La forma simple es insuficiente porque solo es singular: entre Ax y By.

#### B. Forma total o desplegada del valor

Ax = By = Cz

Translado de lo singular a lo general.
  - El intercambio no regula la magnitud del valor, sino al contrario.
  - Toda mercancía es equivalente.

Deficiencias:
  1. Es incompleta porque no tiene término (Cz...Nn).
  2. Tiene expresiones de valor divergentes y heterogéneas.
  3. El trabajo útil tiene forma particular, aunque en modo global se expresen
     de forma plena.

#### C. Forma general de valor

(Ax v By) · ~(Ax · By) = Cz \
Ax ⊻ By = Cz

El valor ahora es:
  1. Simple, representado en una sola mercancía (Cz).
  2. Utilitaria, hay un valor común a todas (Cz).
=> Expresa una mercancía como separada a los demás, para representar el valor
   de otras mercancías.
  - Implica una forma social vigente.
  - Es la reducción de todos los trabajos reales.
  - Expresa a sí misma su valor.
    - Problema: tautología (Cz = Cz).

#### D. Forma de dinero

Ax ⊻ By ⊻ Cz = Dd

Se crea un equivalente general edificado por costumbre social.
=> Oro como dinero.
  - El oro deja de ser mercancía y se empieza a tratar como dinero.
  - Forma general de valor se convierte en forma de dinero.

### 4. El carácter fetichista de la mercancía y su secreto

A primera vista la mercancía parece cosa trivial, pero su análisis demuestra
que es rico en sutilezas metafísicas y reticencias teológicas.
  - Valor de uso no es misterioso.
  - Valor de cambio no es misterioso.
=> Se da en la forma misma de la mercancía.
  - Refleja la relación social entre productores y el trabajo social como
    relación social entre objetos.
    - Forma fantasmagórica, como figura religiosa por parecer estar dotada
      de vida propia.
  => Fetichismo:
    - Origen de la índole social del trabajo.
    - El intercambio las pone como relación entre cosas.
    - La equiparación se da como comparación del trabajo humano.

El cristianismo es la forma más adecuada por su culto al hombre abstracto
y su desenvolvimiento burgués.
  - Ilusión de que la naturaleza de las cosas tiene un papel en la formación
    del valor de cambio.
  - Supone que el valor de uso se da sin intercambio y el valor de cambio
    solo durante el intercambio.
