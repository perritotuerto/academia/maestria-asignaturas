# *Ordo analogiae*, Mauricio Beuchot

## III. El ícono a través de la historia

### 1. Introducción

La iconocidad puede ser útil para el replanteamiento de la ontología.
  - No dejaría de lado lo particular ni el devenir.
=> El ícono es algo particular que funciona como universal, lo temporal que
   detiene el tiempo.

### 2. El Renacimiento

El ícono como *poiesis* en el Renacieminto se da en la idea de hombre como
microcosmos.
  - La analogía micro-macrocosmos lleva a la cosmología y cosmogonía.
    - Viene desde la Antigüedad hasta el Barroco.

Otra analogía es la del libro de la naturaleza o de la revelación.

### 3. El Barroco

La iconicidad forma parte importante del arte barroco.
  - El ícono es el signo que llega hasta lo profundo.
  - El Barroco es el arte de las profundidades.
=> Se va de la superficie a lo profundo.
  - Produce una metafísica metafórica.
    - Se vive la paradoja de lo temporal y lo eterno.
    - Descentralización.
    - Parte de lo humano.

### 4. Los románticos y la iconicidad

La iconocidad se da en la correspondencia entre las cosas y del hombre con las
cosas.
  - El hombre como universo => Visión dinámica de las cosas.

Se vive en el contraste y la paradoja felizmente.
  - Unión dialéctica entre el intelecto y la voluntad, y entre la razón y la
    fantasía.
  - Insatisfacción por lo dado y aspiración a lo más elevado.
    - Paso de la naturaleza a Dios.
=> Hermenéutica de la facticidad no literal / alegórica.
  - Conjunción de la filosofía y la poesía.
  - Uso de lo ironía.

### 5. Pierce y la iconicidad

Pierce analizó el signo icónico y su abstracción hipostática.
  - El ícono como signo análogo dividido en:
    1. Imagen: la analogía es muy pegada pero no unívoca.
    2. Diagrama: Semejanza más dinámica y representadora de relaciones.
    3. Metáfora: remotamente semejante y representante de funciones no 
       equívocas.

Abstracción hipostática: cosificar algo abstracto, la abstracción en el reino
de las cosas concretas.
  - Idea de modelo o paradigma.

Distinciiones de Pierce:
  - *Type*: muestra o modelo, lo universal de algo individual.
  - *Token*: réplica o modelado, lo particular.
=> Operación de la analogía.
  - Poder abstractivo y universalizador de la iconicidad.
  - El ícono como individual con un carácter universal (abstracto).
    - Universal concreto.
    - Parecidos de familia, respecto a un paradigmo.

### 6. La iconicidad en la fenomenología

Scheler se mueve en la idea del hombre análogo del mundo pero lo conecta con el
concepto de sublimación.
  - El hombre resume en sí mismo los estadios de la existencia.
  - La iconicidad como regresión.

### 7. La iconicidad y Heidegger

Obligación de captar lo emergente, lo que está en movimiento conceptual.
  - Ir a los orígenes del pensar: los presocráticos.
=> Heidegger usa la fenomenología como estructura trascendental del pensamiento
   y la hermenéutica como acceso al ser.
  - Asociación de lo óntico a lo ontológico.

La iconicidad es necesaria para esa asociación.
  - Permite detener el tiempo sin falsearlo.
    - Recuperación necesaria que canceló Heidegger.

### 8. Ejemplo de iconicidad ontológica

El filósofo es caminante preocupado por lo que sucede en el viaje y durante
el camino.
  - Sortean los entes, quieren fusionarse con el ser.
    - La iconicidad sirve para que la ontología tenga vida.

### 9. El ícono y la ontología

A la ontología se le aplica:
  1. Analogía de proporcionalidad.
  2. Analogía de atribución.
=> La analogía es iconicidad.
  - Lo que se recoge en la ontología es que el ícono parte de lo dado pero
    es un modelo, un ideal.

Ontología como hecha a base de modelos que permite captar al ser más allá de
lo ente.
  - El uso de la hermenéutica analógica-icónica.

El ícono no se puede captar sin la imaginación y avanza a lo intelectivo.
  - Ayuda a comprender al ser y el ser que más nos importa: el hombre.
  - Ofrece una nueva manera de pensar.
    - Evita falsa abstracción o mala totalidad.

### 10. Conclusión

La iconicidad permite una ontología analógico-icónica.
  - Revitalización significativa para el hombre.

## IV. Analogía e iconicidad en Vico

### 1. Introducción

Giambattista Vico es un antecedente de la hermenéutica analógica-icónica.
  - No la llamó con este nombre.
  - La sabiduría usa modelos de una historia ideal reflejada en lo concreto.
    - No es relativismo, buscó cientificidad.
    - No idealismo, buscó principios y leyes realistas.
    - Analógico-icónico, da lugar a la fantasía.
    - Busca cultivar la sensación y la imaginación.
=> Crítico de la modernidad con su realismo poético.

### 2. Vida y obra

Datos:
  - Nació en Nápoles en 1668.
  - Estudió filosofía escolástica y derecho.
  - Fue profesor de retórica.
  - Perdió la memoria.
  - Murió en 1744.
=> En el siglo XIX su pensamiento fue rescatado.
  - Obra relevante: *Ciencia nueva*.

### 3. El conocimiento: ciencia divina y ciencia humana

La verdadera filosofía se encuentra en la sabiduría italiana a partir de su
estudio de los orígenes de la lengua latina.
  - Su análisis lingüístico le hizo ver que la filosofía no tiene un método
    único.
    - vs. Descartes y su exageración del método matemático y menosprecio de la
      historia y la filología.

Oposición:
  - Razón demostrativa de Descartes le corresponden la crítica, se basa en lo
    verdadero y es deductiva.
  - Ingenio inventivo de Vico le corresponde la tópica, se basa en lo verosímil
    y es inductiva.
=> La verdad es el hecho: cosas hechas por el hombre y por Dios.
  - La naturaleza fue hecha por Vico, por lo que solo él la conoce, lo nuestro
    son aproximaciones.
  - La ciencia está hecha por el hombre a través de ficciones convencionales.
    - Incluida la historia.
=> A Dios le pertenece el *intelligere*, la intuición, y al hombre el 
   *cogitare*, el raciocinio.
  - Recoger elementos para poco a poco edificar el saber.
  - El hombre solo puede conocer su existencia.

En lahistoria coincide el *factum* y el *verum*, por lo que la filosofía de la
historia toma relevancia.
  - Dos historias que corren parejas:
    1. La ideal o eterna regida por la Providencia sin cronología.
    2. La concreta y particular de las naciones que avanza con arquetipos que
       la conectan con la ideal.

### 4. Dios: la Providencia

Dios en la historia universaldesde donde se realizan historias particulares.
  - No casualidad o fatalidad.
  - Sí orden y libertad.
=> Movimiento sin determinar.
  - Algunas naciones no han seguido su curso, siendo causas de desastres.
  - El Logos se vuelve dinámico y con intenciones de concretud.

La historia tiene un finalidad más allá de la comprensión de los hombres.
  - El sentido es la elevación del hombre por Dios, siendo este su fundamento
    metafísico.

### 5. Filosofía de la historia: los retornos

La nueva ciencia es la historiografía.
  - Física es saber disminuido => Límite a Galileo.
  - Matemática es ficticia => Freno a Descartes.
=> Conocimiento del hombre en su circunstancia y condición.
  - Conocer lo que el hombre ha hecho.

Vico usa un esquema ternario (que Comte recogerá) que pasan al hombre de la
barbarie a la civilización.
  1. Edad de los dioses: predominan la sensibilidad y el sentido común, donde
     las fuerzas de la naturaleza son dioses que castigan, su lenguaje es 
     sagrado y jeroglífico con forma poética y régimen teocrático.
  2. Edad de los héroes: privilegia la imaginación y la fantasía en un régimen
     aristocrático, donde el derecho está basado en la fuerza y con una
     sabiduría poética expresada en el lenguaje metafórico.
  3. Edad de los hombres: de la razón y la reflexión con un régimen democrático
     y con un derecho basado en la razón y el deber cuya sabiduría es reflexiva
     y expresada en un lenguaje letrado y clásico.
    - Mina a la religión y provoca un regreso a la primera etapa.
      - Retorno que no es eterno retorno ni dialéctica hegeliana.
=> Es muy esquemático: concepción apriorista e idealista.

### 6. La sabiduría poética

El salvaje fantasioso da origen a la sabiduría.
  - Sabiduría poética: no es abstracta sino a base de imágenes.
  - La metafísica poética se da en el mito.

A la fantasía le sucede la razón.
  - Su movimiento se conserva.
  - La razón ordena las pasiones mediante leyes.

La sabiduría poética da lugar a los universales fantásticos:
  1. La que corresponde a la edad de los dioses y cuyo carácter poético
     corresponde a fenómenos naturales o instituciones.
  2. La que corresponde a la edad de los héroes cuyo caracter poético 
     corresponde a la valentía.
=> Proviene de un acto creativo.
  - Con una memoria como fantasía que descubre su propio origen.

2 sentidos de fantasía:
  1. Como instrumento de sabiduría poética.
  2. Como lo que hace comprender al mundo humano por medio del recuerdo.
=> El mundo humano se conoce porque se aprehende como recuerdo en el tiempo.

En la tercera edad los universales son abstractos.
  - Se pierde la capacidad de conocimiento concreto.
  - El sentido común como un juecio sin reflexión.

### 7. Reflexión

La sabiduría de los latinos es poética, basada en los universales fantásticos
que a su vez son concretos.
  - Es el ícono cuya representación es más significativa para el hombre.
=> Realismo hermenéutico analógico-icónico.
  - Realismo hermenéutico porque no confunde las cosas.
  - Hermenéutica analógica porque no diluye la metafísica en la lógica.

### 8. Conclusión

Realismo, hermenéutica y poesía para atrapar la realidad de las cosas con la
razón y potenciarlas con la fantasía.

## VIII. Sobre el sentido de la vida, desdela hermenéutica analógica

### 1. Introducción

El sentido se encuentra buscando íconos y símbolos
  - La pregunta sobre el sentido de la vida es evitado.
=> La filosofía tiene que recuperarla.
  - La hermenéutica analógica puede ayudar.

### 2. La eterna pregunta sobre el sentido de la vida

La religión y el origen de la filosofía se han planteado la pregunta.
  - Cada quien es constructor de su sentido.
    - Un proyecto de vida no es suficiente.
=> El amor como sentido.

### 3. La situación actual sobre el sentido de la vida

La filosofía marxista o analítica daba sentido, pero ahora han decaído.
  - Señalado por posmodernos antimodernistas.
=> El placer como falsa dadora de sentido porque genera un vacío.
  - Necesidad de una mediación entre analíticos y posmodernos.

### 4. Interpretar la vida

Buscar sentido = interpretar.
  - Interpretar es hacer ontología para desembocar en antropología filosófica.
=> Necesidad de hermenéutica analógica para juntarlas.

### 5. Leer la vida analógicamente

La hermenéutica analógica evita las limitaciones de las unívocas o equívocas.
  - Lleva a cabo una dialéctica que no sintetiza sino que permite convivir a
    los opuestos de manera frágil.
    - Aceptación de la diversidad de ideas.
    - Brota un tercer elemento que sustenta la tensión.

### 6. Conclusión

En el sentido de la vida es donde más senecesita la hermenéutica analógica.
  - La amistad y el amor alcanzan su máxima expresión para dar sentido a la
    vida.
