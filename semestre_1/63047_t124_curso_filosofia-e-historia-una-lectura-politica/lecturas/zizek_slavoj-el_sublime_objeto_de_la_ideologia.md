# *El sublime objeto de la ideología*, Slavoj Zizek

## Parte primera. El síntoma

### 1. ¿Cómo inventó Marx el síntoma?

#### Marx, Freud: el análisis de la forma

Según Lacan fue Marx quien inventó el síntoma.
  - Homología entre Marx y Freud.
    - Freud: 1) el sueño es un fenómeno significativo y 2) hay que deshacer la
      fascinación que provoca para centrarse en la forma.
    - Marx: 1) la mercancía tiene un significado oculto y 2) hay que abandonar
      la fascinación que provoca para ver el misterio de su forma.
  => Análisis del proceso de formación.

#### El inconsciente de la forma mercancía

El análisis de la mercancía ofrece una clave para la comprensión de los 
fenómenos mediante la idea de la «inversión fetichista».
  - Ejerce poder de atracción.
  - Se encuentra el sujeto trascendental.
    - Paradoja: el fenómeno del munto interior ofrece la clave para resolver la
      pregunta sobre la validez universal del conocimiento objetivo.
  => El aparato de categorías ya está presente en la efectividad social: en el
     acto de intercambio de las mercancías.
    - En el dinero ya hay una determinación puramente cuantitativa.
    - En el intercambio ya hay un movimiento puramente abstracto.
  => La cualidad de las mercancías pasan a un segundo plano.

El escándalo: la génesis del sujeto trascendental depende del mundo interior.
  - La abstracción real es el inconsciente del sujeto trascendental.
    - La abstracción real es una forma de actuar como si no hubiera
      determinaciones materiales a pesar de que se es consciente de ello.
  - Es un proceso que no tiene lugar en el interior del sujeto sino en un acto
    de intercambio.
    - No es pensamiento sino que tiene forma previa de ello.
    - Definición posible del inconsciente.
=> Implica un material «sublime» con consistencia indestructible e inmutable.
  - Corporalidad inmaterial dependiente de una autoridad de orden simbólico.

<!-- Pensar la información sin infraestructura física es una abstracción 
     real -->

El escándalo en la filosofía es que la reflexión filosófica ya implica un lugar
externo donde su forma ya ha sido puesta en escena.
  - Sometida a una experiencia misteriosa.
  - Definida por su ceguera sobre este lugar.
=> Una función análoga se da en la «conciencia práctica».

La efectividad social del intercambio precisa que sus participantes no sean
conscientes de su propia lógica.
  - Dimensión fundamental de la ideología.
    - No es falsa conciencia.
    - No es una representación ilusoria de la realidad.
    - Es una realidad ya concebida como ideológica.
    - Es una realidad que implica un no-conocimiento de sus participantes en lo
      referente a su esencia.
  => Síntoma social.
    - Se goza en la medida que se la ignora.
    - Se diluye en la medida que su lógica se muestra.

#### El síntoma social

Marx detectó una fisura que desmiente el privilegio de los burgueses.
  - El síntoma que es un elemento particular necesario que al mismo tiempo
    subvierte su fundamento universal.
=> Crítica sintomática: detectar un puto de ruptura heterogéneo en un campo
   ideológico determinado que a su vez es necesario para lograr su forma 
   acabada.
  - Lógica de la excepción: cada universal es falso en la medida que incluye
    un caso necesario que rompe su unidad.
  - Utopía: posibilidad de universalidad sin síntoma.
=> En la sociedad como totalidad racional el proletariado es su síntoma: la
   sinrazón en la razón encarnada.

#### Fetichismo de la mercancía

Lacan ve el síntoma atribuido a Marx en el pasaje del feudalismo al capitalismo.
  - Para captar la lógica del pasaje es necesario analizar el fetichismo de la
    mercancía.
    - Fetichismo de la mercancía: el valor de una mercancía, fruto de una red de
      relaciones sociales, asume un valor casi natural de otra mercancía, el
      dinero.
=> El rasgo esencial es que el efecto de la red de relaciones parece una
   propiedad inmediata de unos elementos como si este estuviera fuera de la red.
  - La mercancía equivalente es un reflejo aunque se ven como ser en sí un
    equivalente.

Con la determinación refleja se entiende que un estatus social no se da en sí,
no es natural, sino que es un efecto de una red de relaciones sociales.
  - En el fetichismo se da de una forma invertida, ese supuesto en sí, que se da
    en dos formas:
    1. Fetichismo de la mercancía donde toda la gente es «libre».
    2. Fetichismo en las relaciones entre los hombres donde la mercancía se da
       como natural y la relación entre los hombres es mediante la servidumbre.
=> Incompatibles.
  - La sociedad capitalista reprime a la primera donde el síntoma de los hombres
    «libres» es la apariencia ideológica de la igualdad y la libertad.
    - La relación entre hombres es disfrazada por la relación social entre las
      cosas.

#### Risa totalitaria

La ideología imperante no quiere ser tomada en serio o literalmente.
  - Incluso supone un peligro a la misma ideología.
  - La risa forma parte de la ideología.

#### El cinismo como forma de ideología

Definición básica de ideología: «Ellos no lo saben, pero lo hacen».
  - Supone ya un distanciamiento entre la realidad social y la representación
    distorsionada.
    - Posibilidad de que la conciencia ingenua pueda someterse a un 
      procedimiento crítico.
=> Versión más sofisticada: la realidad no puede reproducirse sin la 
   mistificación ideológica.
  - ¿Continúa siendo aplicable?

Sujeto cínico: «Ellos saben muy bien lo que hacen, pero aun así lo hace».
  - No es conciencia falsa sino una que sigue a sabiendas de los intereses
    ocultos de la universalidad.

Kinismo: rechazo popular a la cultura oficial por medio de la ironía y el
sarcasmo; es más pragmático que argumentativo.

Cinismo: respuesta de la cultura dominante al kinismo; reconoce sus defectos
pero aun encuentra razones para conservar su máscara; la moralidad al servicio
de la inmoralidad; «negación de la negación».
=> La crítica tradicional a la ideología deja de funcionar, es la lectura
   sintomática.
  - La ideología ya no pretende ser tomada seriamente sino como medio de
    manipulación instrumental: su valor no reside en su verdad sino en la
    simple violencia y promesa de ganancia.

#### Fantasía ideológica

La ilusión ideológica supone que reside en el «saber» y no en el «hacer».

Reificación: detectar las relaciones sociales tras la relación entre las cosas.
  - vs: la mayoría de las personas ya lo «saben» pero «hacen» que no importada;
    fetichismo solo en la práctica.
=> La ilusión está del lado del «hacer».

La ilusión es doble:
  1. Se sabe cómo son las cosas pero se obvia.
  2. Se para por alto cómo se estructura a nuestra realidad efectiva.
=> Fantasía ideológica: ilusión inconsciente que estructura nuestra realidad que
   sin importar la distancia irónica aun así se hace.

#### La objetividad de la creencia

El fetichismo de la mercancía fue destimado por althusserianos por dar una
posición ingenua entre personas y cosas.
  - Para la lectura lacaniana en esto reside la capacidad subversiva del enfoque
    marxiano.
    - No son los hombres sino las cosas las que creen en mistificaciones.
  => Son funciones delegadas a las cosas, por lo que se vuelven objetivas.

#### «La ley es la ley»

La creencia no es un estado íntimo, se materializa en la actividad social.
  - La realidad social es una construcción ética en cuya pérdida de creencia se
    desintegra.
    - La obediencia a la ley no es por nuestra subjetividad —que la consideramos
      buena—, sino por mera exterioridad: la ley es la ley.
=> Condición positiva: su carácter traumático, sin sentido.

No hay que aceptar la ley como verdad sino como necesaria.
  - Tranferencia: suponer una verdad detrás de un trauma.
    - Es un círculo vicioso porque las razones de creer en ello solo son
      convincentes para lo que ya creen.
  - Actuar como si cree para que por medio del ritual se termine creyendo.
=> Conversión ideológica.
  - En un universo donde todos buscan verdades debajo de máscaras, se 
    descarrilan al ponerse una máscara de la verdad.

#### Kafka, crítico de Althusser

La máquina externa decide de antemano a nuestras creencias internas.
  - Althusser da una versión contemporánea de esta idea pascaliana.
=> Tiene un problema: no explica cómo se internaliza el Aparato Ideológico del
   Estado.
  - Problema de la interpelación ideológica.

Kafka permite ver la constitución de esa brecha.
  - Interpelación sin subjetivación.

La ideología no es un sueño construido para huir de la insoportable realidad,
sino una función básica que sirve de soporte, una construcción fantasiosa.
  - No es punto de fuga.
  - Es la realidad misma como huida de algún trauma.

#### La fantasía como soporte de la realidad

El acercamiento la núcleo duro de lo Real es mediante el sueño.
  - Para romper el sueño ideológico es necesario confrontar lo Real de nuestro
    deseo.

La ideología toma partida cuando no hay oposición entre ella y la realidad.    <

#### Plus valor y plus-de-goce

En la perspectiva marxista predominante la mirada ideológica se considera
parcial, ya que no mira la totalidad de las relaciones sociales.
  - En la perspectiva lacaniana la ideología es una totalidad que borra las
    huellas de su propia imposibilidad.
=> Relación a la noción de fetichismo:
  - En Marx oculta la red positiva de relaciones sociales.
  - En Freud oculta la falta de la articulación simbólica.

Existe otra diferencia:
 - En el marxismo el procedimiento ideológico se da mediante la falsa
   universalización.
 - En los lacanianos la ideología es una historización superrápida para
   invisibilizar al núcleo que retorna como lo mismo.

El marxismo no logra tomar en cuenta aquello de lo Real que elude la
simbolización.

Plus-de-goce: el goce es constitutivamente un excedente, el objeto-causa del
deseo.
  - Homología al plus valor al ser este la «causa» que pone en movimiento al
    proceso capitalista de producción.

### 2. Del síntoma al *sinthome*

#### La dialéctica del síntoma

##### Regreso al futuro

Los síntomas son huellas sin sentido cuyo significado se cosntruye 
retroactivamente, no excavando en el pasado.
  - El pasado está presente como tradición histórica cuyo significado cambia   <
    constantemente.
    - Las rupturas son regisnificaciones.

Los viajes al futuro son transferencias.
  - Transferencia: actualización de la realidad del inconsciente.

El pasado existe en la medida que entra en la red significante.                <
  - En la medida que es simbolizado en la memoria histórica.
=> La historia se reescribe.
  - Producción simbólica del pasado: de los sucesos traumáticos olvidados.

Paradoja:                                                                      <
  1. El sujeto confronta al pasado que ya «no puede cambiar».
  2. A través de su intervención el pasado se «convierte en lo que siempre fue».
=> La «ilusión» reside en que el sujeto pasa por alto su inclusión en la escena.
  - La verdad surge de este falso reconocimiento.

##### Repetición en la historia

De nuevo la paradoja:
  1. El error llega antes de la verdad.
  2. Por la verdad se hace posible reconocerlo como error.
=> Lógica de la «astucia» del inconsciente.
  - El inconsciente no es trascendente, sino un hacer la vista gorda.
  - La transferencia como ilusión esencial para producir la Verdad: el 
    significado del síntoma.

La repetición histórica es necesariapara lograr el objetivo.
  - La Verdad surge del fracaso, del errar.
  - La repetición es el modo como la necesidad histórica se afirma a los ojos de
    la «opinión».
    - Se constituye a través de un falso reconocimiento.
    - La necesidad es simbólica, no algo objetivo.
    - La interpretación viene cuando el acontecimiento a interpretar se repite.
    - Implica la experimentación de un trauma que luego se percibe como una
      necesidad simbólica.

##### Hegel con Austen

Jean Austen como la contrapartida literaria de Hegel.
  - *Orgullo y prejuicio* como dialéctica del falso reconocimiento.
    - No se va directamente a la verdad, sino mediante el falso reconocimiento.

##### Dos chistes hegelianos

Un chiste que ilustra cómo la verdad surge del falso reconocimiento.
  - Sobre el polaco y el judío.

Otro chiste con la misma estructura.
  - Presente en el noveno capítulo de *El proceso* de Kafka.

##### Una trampa del tiempo

El falso reconocimiento es una instancia «productiva» que ya trae consigo la
posibilidad de una entidad positiva.
  - Su abolición es la disolución ontológica.

#### El síntoma en tanto que real

##### El *Titanic* como síntoma

La dialéctica de rebasarnos hacia el futuro y la modificación retroactiva del
pasado tiene sus límites.
  - Es lo Real, lo que resiste a la simbolización.
    - El Titanic como esa Cosa aterradora y la metáfora como un intento de
      evadirlo.

##### Del síntoma al *sinthome*

El mundo, el lenguaje y el sujeto no existen, solo el síntoma.
  - Universalización del síntoma.
    - Captarla mediante la forclusión: fenómeno específico de la exclusión de un
      determinado significante clave.

Existe una forclusión en el significante en tanto tal.
  - La estructura simbólica está estructurada en torno a cierto vacío de un
    significante clave.

*Sinthome* es una formación significante penetrada de goce con lo que se evita
la locura.
  - Se escoge el síntoma en lugar de la nada.
  - El síntoma es una mancha que resiste a la comunicación y a la 
    interpretación.

##### «En ti más que tú»

El *sinthome* es «psicosomático».
  - Una especie de parásito que todo se vuelve peor si se elimina.
  - La verdad y el goce son incompatibles.

##### *Jouissance* ideológica

El anverso necesario de la ilustración: la obediencia a usos y normas.
  - P. ej. el imperativo categórico: obediencia a una ley que no es verdad pero
    es necesaria.
    - Un hecho trascendental cuya verdad no se puede demostrar.

El poder ideológico reside en el vacío y la formalidad: obediencia y sacrificio
porque sí.
  - Sirve a sus propios objetivos, no sirve para nada.
  - Lo único que está en juego es su forma.
=> Definición lacaniana de *jouissance*.
