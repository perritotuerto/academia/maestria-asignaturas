# *El péndulo y la espiral*, Ramón Xirau

## I. Economía y espíritu

Idea del progreso nace en la burguesía industrial.
  - Patrimonio de los pensadores del siglo XIX.
=> La novedad en Marx es su actitud ante el progreso.

En la filosofía hay tiempos de innovaciones y otros de síntesis.
  - Tres periodos de acarreos:
    1. Tales - Sócrates => Platón y Aristóteles.
    2. San Agustín y pensadores árabes y judíos => Tomás de Aquino.
    3. Racionalismo y empirismo, y Kant => Hegel.
=> Las sumas son enormes bloques pero también pensamiento que empieza a decaer.

En Marx lo absoluto es la economía.
  - Determinismo en las relaciones entre las clases sociales.
  - La historia pensada como constante conflicto.
=> Puede ser falso pero es base pragmática de la acción.
  - Necesidad de una historia de la filosofía donde se explique que las causas
    económicas son la base de todo lo humano.
    - Explica la injusticia con la teoría de la plusvalía y la acumulación de
      capital.
    => Una ley para el mundo del hombre.
  => Es una abstracción, por ende, una metafísica.
    - No implica que sea desechable.

Ciencia y filosofía mecanicista = péndulo.
  - El péndulo marxista le dio impulso al obrero.
    - La revolución como inevitable, justa y verdadera.
    - Metafísica donde las relaciones humanasson el absoluto: paraíso terrestre
      para y por el hombre.

Artistas como expresión y condicionados a su tiempo.
  - Tautología, sin sentido o falsedad.
  - Más bien lo trascienden.
=> El arte no progresa, más bien ingresa a una región más alta.
  - El marxismo no puede explicarlo.

En ciertamedida Marx tiene fe en el progreso al convertir la historia en un
mecanismo hacia un ideal.
  - No es verdadero progreso, eso no es mecánico sino espiritual.
  - Absolutización de lo relativo.
=> Doble contradicción:
  1. Marxismo como filosofía de lo concreto que se hace abstracto.
  2. La injusticia hecha utopía.

## II. La estática social y dinámica histórica

La filosofía de la historia de Comte interesa más por lo que simboliza que
por lo que vale.
  - Tiene antecedentes franceses.
    - Saint Simon.
    - Enfantin.

La historia se desenvuelve en 3 etapas:
  1. Teológica: fetichismo, magia y religión.
  2. Metafísica: abstracción y explicaciones imaginarias.
  3. Positivo: explicación definitiva y científica de los hechos.
     - Alcance de la felicidad.

Comte imagina que hay leyes sociales a modo de leyes físicas.
  - Los tres estadios no resisten el análisis histórico.
=> Filosofía pendular donde la ley social es suprahistórica.

## III. Las estaciones

Spengler como otro péndulo.
  - Las civilizaciones como organismos vivientes.
=> *La decadencia de Occidente* es una descripción de esta morfología.

Ritmo de las estaciones:
  - Primavera: nacen mitos, metafísica y misticismo.
  - Verano: maduración de la conciencia y primeros centros urbanos; nacimiento
    de la filosofía.
  - Otoño: las civilizaciones se vuelven urbanas, culmen del pensamiento
    matemático y desarrollo de grandes sistemas de pensamiento.
  - Invierno: decadencia de la civilización, etapa de ideales morales y de la
    filosofía escolar.
=> Ideal de simetría biológica y reducción a términos occidentales.
  - Historia occidentalizada.

Semejanza con Marx y Comte:
  - Ideal de simetría.
  - Filosofías de la historia deterministas.
  - Inútiles en su totalidad.

## IV. La energía espiritual

Bergson: rebelde ante las interpretaciones mecanicistas de la historia.
  - La experiencia no es reducible a la interpretación de datos de la
    sensibilidad.
    - Se extiende a la experiencia religiosa.

En las teorías deterministasel futuro ya está preestablecido.
  - Ajena a la libertad como abertura y dinamicidad.
=> Moral abierta: dinamismo que sigue lo que el hombre va trazando mientras
   vive.
  - Los místicos son sus creadores, que a su vez crean doctrinas.
  => Resulta en una sociedad abierta: amorosa y mística.
    - La religión como su impulso.

Para Bergson la historia es historia de la mística.
  - La verdadera mística es la mística cristiana.
  - La historia como aventura del espíritu.
=> Si la historia y la filosofía de la historia es una cuestión de vida y un
   impulso creador, Bergson es el único que ha escrito una filosofía.
  - El hombre con libertad de entretenimiento para llegar a conocer a Dios.

## V. Ambigüedades del siglo XX

La historia adquiere su plenitud en el presente.
  - Sucede en la vida del espíritu: cambio y devenir.

En Anaximandro es la primera vez que se presenta la oposición entre dinamismo
y estaticidad.
  - Problema del cambio: Heráclito y Parménides.
=> Platón como síntesis.
  - Sin separar la idea de tiempo con la de cambio y movimiento.

Aristóteles empieza a separar la idea de tiempo y a clarificarla.
  - No vio que el tiempo es tiempo humano.
=> La noción de tiempo nace en el cristianismo.

Cristianismo: el mundo ha sido creado de la nada = el mundo tiene un comienzo
en el tiempo.
  - El mundo ha nacido y tiene que morir.
  - San Agustín inicia el análisis moderno de la termporalidad.
    - El tiempo real es la memoria y la previsión: tensión.

La filosofía moderna acentúa la idea cristiana de temporalidad.
  - La mayor claridad la alcanza Leibniz.

La idea de progreso penetra muchas de nuestras acciones.
  - Una forma es el vanguardismo donde se reduce lo mejor en lo último.

Afirmación del futuro = afirmación como fin en sí.
  - P. ej., Heidegger y Huxley.
=> Una reacción es hacer del presente un absoluto.
  - P. ej., Sartre.
    - Para sí = realidad cambiante.
    - En sí = esencias.
  => El para sí nunca alcanza el en sí.
    - No es posible fundar el hombre en algo que lo trascienda, por lo que su
      existencia se funda en el presente.
    => El hombre debe estar comprometido => vs: pero no todo hombre puede
       decidir su destino.

La primacía del presente convierte al hombre en cosa, se institucionaliza.
  - «hombre-organización» de Whyte.
    - Aguanta y se aguanta.
    - Actividadcomo uso de sí mismo como instrumento.

Otra opción es volcarse al pasado.
  - Suele llevar un signo mítico.
  - Tradicionalismo que va en contra de las tradiciones al negar en ellas su
    potencial de transformar el futuro desde el presente.

La disolución de la idea del tiempolleva a fariseísmo o protesta.
  - Fariseo siempre piensa que está del lado de la justicia: la razón es su
    razón.
    - Hipócrita y cobarde.
    - Sentimiento de superioridad.
  - La protesta puede ser fructífera pero también ambigua.
    - Afirma valores humanos.
    - Relativista.
    - En camino, no un fin.

## VI. Quid est veritas?

¿Qué es la verdad?
  1. Es objeto del conocimiento.
  2. Es una forma de ser.
=> Las escuelas lógicas no lo piensan así.

En Wittgenstein el único lenguaje posible es el constituido por proposiciones
significativas = lenguaje lógico.
  - Lo importante son los «hechos atómicos».
=> Escepticismo radical donde se reduce el conocimiento a la posibilidad de
   decir algo.

La verdad es Ser y Ser significa Dios.
  - Inefable.
  - No se tiene.
=> Mística: progreso como encuentro con el espíritu.
  - También es ascenso de la verdad.
