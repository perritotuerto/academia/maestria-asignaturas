# 7 de agosto

rosauramartinez@gmail.com
Edmodo o #filnat por TW

Cada vez se ven más ontologías continentales preocupadas por lo vivo
y los animales.
  - Ver qué ha sucedido para que este tema sea actual.
  - Pensar las condiciones de posibilidad del tema.
  - Involucra a la antropología filosófica, ya que hace replantear
    nuestro lugar en el cosmos.
=> Empezar por Arendt.

La principal línea de investigación de Butler: biopolítica.
  - Marcos epistemológicos que deciden qué vidas valen defender y 
    cuáles no.

La filosofía política está dando un giro psicoanalítico.
  - ¿Cómo es posible pensar una psique colectiva que regula la vida
    colectiva?

Foucault para poder pensar la vida junto con la historia y las
instituciones.

Para la evaluación es un ensayo máx. de 5 cuartillas cada módulo.

Para la próxima clase: prólogo y cap. 1 de *La condición humana*.

# 14 de agosto

No asistí, me fui a Oaxaca con De.

# 21 de agosto

¿Cuáles son los temas centrales del argumento de Arendt?
  - Está haciendo una antropología filosófica desustancialista, no 
    naturalizada: la condición humana no está dada; produce sus propios 
    condicionantes.
    * La categoría de «arte» se vuelve fundamental.
=> La condición humana es producida, donde el paradigma es el arte.
  - Crea sentido: la vida humana como algo producido.
  - Condicianada por sí misma, no en términos esencialistas, sino como
    una producción.

Posibilidades de producción:
  - Obra de arte: presenta al mundo como producto de interpretaciones
    y la condición humana como producto de estas interpretaciones.
  - Técnica: produce el mundo y cuestiona al sujeto como causa de la
    síntesis: el sujeto es un resultado no un fundamento.
    * Inclinación de Arendt.
=> Se piensa la producción en términos políticos.

En la sociedad de consumo ya no hay objetos de uso, sino de consumo:
no están pensados para durar.

Leer «La vida activa y la edad Moderna» para la siguiente sesión.

# 28 de agosto

El «hacer» en _La condición humana_:
  - Labor: vida / naturaleza
  - Trabajo: _homo faber_ / mundo de los objetos
  - Acción: política / discurso
=> De lo particular a lo comunitario donde el paradigma no son las artes.

El artificio tiene que pensarse clavado en el mundo natural.
  - Relación con la explotación.
=> No hay artificialidad pura.
  - Rastrearlo hacia sus complementos.

¿Quién ha producido el mundo humano? El esclavo.

# 4 de septiembre

Hubo paro.

# 11 de septiembre

Hubo paro.

# 18 de septiembre

Hubo paro.

# 25 de septiembre

<!-- Tarea: _Más allá del principio el placer_ -->

Conclusiones con Arendt:
  - ¿Quién realiza el trabajo?
  - ¿Qué quiere decir esta sociedad de consumo?
  - Tensión entre _animal laborans_ y _homo faber_.

Leer a Agamben desde la filosofía de la técnica y la vida.
  - El «animal» como la categoría ontológica.
  - Establece un diálogo irresponsable con la biología.
    * No tratasu historia ni sus problemas ni sus categorías.
=> El misterio de la disyunción entre el hombre y el animal.

Hay una hermenéutica animal a partir de su capacidad de significar su
entorno.

La máquina antropogénica necesita una separación.
  - El animal es lo que permite la excepción.

# 2 de octubre

No hubo clases.

# 9 de octubre

_Principio del placer_
  - Le tomó tiempo porque se dio en un periodo porque se dio en un
    periodo entre guerras.
  - ¿Por qué elhombre se inclina a la guerra y a la destrucción?
  - Primeros capítulos: noción conservadora del placer.
=> El placer rige la vida anímica.
  - La _psique_ es una pugna constante.
  - Placer = disminución de la tensión.
  - Es necesaria al menos un mínimo de tensión.
=> El principio del placer nunca es contrariado.

_vs_ placer como búsqueda:
  1. El hombre hace muchas cosas que no le dan placer.

No puede haber cultura sin la regulación de los efectos.
  - Los deseos están cohartados desde el principio.

Definiciones rápidas:
  - Pulsión: afecto, algo que nos mueve.
  - Angustia: no hay objeto repectáculo de la angustia.
  - Temor: hay objeto definido a lo que se teme.

# 16 de octubre

No fui.

# 23 de octubre

No fui.

# 30 de octubre

Tarea: «Los cuerpos dóciles» en _Vigilar y castigar_.

Entrega de trabajo del módulo para antes del 4 de diciembre:
rosauramartinezruiz@gmail.com, máx. 5 cuartillas.

# 6 de noviembre

¿Quién es Foucault?
- Introducción biográfica.

<!-- Constitución del sujeto a partir de lo que está detrás de las cosas:
     sus procesos, su crítica, sus posibilidades, sus limitaciones, sus
     comunidades. No tanto «el trabajo» o «los frutos», sino todo lo que
     se desprende mientras se realiza un trabajo. -->

_Vigilar y castigar_.
- Inicia con un suplicio.

Proyecto moderno: la naturaleza como sustrato de dominio.

Textos para la siguiente clase:
* Michel de Certau, _Historia de cuerpos_.
* Michel de Certau, _Foucault y la muerte del hombre_.
* Michel Foucault, _Cuerpo utópico_.

Texto a revisar por mi cuenta:
* Michel Foucault, _La verdad y las formas jurídicas_.

Correo del profesor: cuitlahuac.moreno@gmail.com.

# 13 de noviembre

Los niños adquieren el significado de su cuerpo a través de las 
caricaturas.
  - Son presos políticos de generaciones anteriores porque se ven
    obligados a reproducir lo que dictan los padres.
=> El cuerpo llega por simbolización.

Trabajo final de 5-8 cuartillas sobre un tema visto en clase.

# 20 de noviembre

No asistí.

# 27 de noviembre

Clase sobre transhumanismo.

Ni Heidegger ni Sloterdijk piensan en la esencia de lo humano.
  - Heidegger: en la tecnociencia existe una posibilidad de
    ya no recurrir a la metafísica o teología.

La técnica como una nueva forma de magia.

Manuela de Barros: el avance a una vida más sintética para mejorarla.
- Posbiologismo, posterriconalismo.

Máquina: un instumento que te permite facilitar tu trabajo.
- Automata: un dispositivo capaz de hacer un trabajo sin poder
  tomar una decisión.
- Robot: requiere algoritmos para poder tomar decisiones.
- Cyborg: simbiosis entre humano y máquina.
  * No tanto para un avance del progreso.
  * No requiere prótesis de orden mecánico.
  * Más como retrospectiva.

Post y transhumanismo: lo humano es un artilugio, es
artificial.
  - Hay que eliminar las limitaciones.

Física de lo real no es igual a la realidad de la Física.
  - Error epistemológico que funda la creencia.

El transhumanismo no habla del futuro, sino del presente.

Ucronía: reconstrucción ficcional de la historia.

Ontológicamente somos cyborgs, no porque estemos compuestos
de máquina, sino por la manera en como interactuamos con
el mundo.
  - Cyborg como mundo medio entre robots y humanos.

Dejar los trabajos en la coordinación.
