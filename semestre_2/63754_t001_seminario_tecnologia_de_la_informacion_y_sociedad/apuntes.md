# 30 de enero

No hubo sesión: error en el horario.

# 1 de febrero

Discusión sobre cómo se va a abordar la materia.

# 6 de febrero

¿Qué se puede definir como TIC? [Respuesta de compañeras]
  * Conjunto de herramientas que permiten la comunicación.
  * Herramientas que permiten interactuar para socializar el conocimiento.
    * Un apoyo para el desarrollo personal.
  * Herramientas o procedimientos que sirven para estandarizar 
    información con el fin de mejorar la vida de las personas.
  * Herramientas que los seres humanos han adoptado y modernizado
    para realizar todas las actividades.

¿Qué es «tecnología»? [Respuesta de compañeras]
  * Comienza desde que el ser humano existe y enfrenta las necesidades
    de su vida diaria.
  * Viene asociado a la innovación, es aquello que va evolucionando
    para ser algo nuevo.
  * Artefactos que el hombre crea para el bienestar, también es
    conocimiento acumulativo.   
  * No solo para satisfacción personal, sino también para modificar
    su entorno.

La tecnología es distinta de la técnica. [Respuesta de compañeras]
  * La técnica son los procedimientos, la tecnología es el instrumento.
  * La técnica está asociado al conocimiento, la tecnología como un 
    producto de esa técnica.
=> Están relacionadas

¿Qué es la «técnica»? [Respuesta de compañeras]
  * Conjunto de conocimientos.
  * Saber hacer algo.

Solo se está considerando cómo a través de actividades repetitivas
el hombre logra establecer una técnica.

La técnica y la tecnología son diferentes.
=> Faltan más lecturas para seguir con la conversación.
  * La creatividad es importante.
  * Es sobre un conocimiento de algo.

# 15 de febrero

Regreso a qué es la tecnología.
  * Requiere de conocimiento científico y, por ende, de un método.
    * Los accidentes y la intuición quedan dentro de la producción del
      conocimiento científico.

# 20 de febrero

No hubo sesión.

# 27 de febrero

No hubo sesión.

# 6 de marzo

No hubo sesión.

# 13 de marzo

Primer tema: cibernética.
  - Norbert Winer como su «padre».
  - Viene del griego que significa «timonel», «dar sentido», «dar estabilidad».
  - Ciencia que mantiene el orden de un sistema.

Segundo tema: Vannevar Bush.
  - Científico estadounidense que creó los selectores rápidos.
  - Propuso «memex»: almacenar toda la información para su consulta.
=> Antecedente del hipertexto.

Tercer tema: Douglas Engelbart
  - Inventó el *mouse*, primero en hacer videoconferencia.
  - Influenciado por Bush.
  - Busca intercambio de ideas y socialización del conocimiento.
  - En pos del código abierto.

# 3 de abril

Ted Nelson:
  * Hipertexto.
  * Hipermedio.
=> Aún utópico, ya que requiere completo acceso a la información.

# 10 de abril

No hubo clase.

# 17 de abril

Crítica sobre los trabajos finales.

# 24 de abril

No hubo clase.

# 1 de mayo

Día asueto.

# 8 de mayo

No fui.

# 15 de mayo

Día asueto.

# 22 de mayo

No hubo clase.
