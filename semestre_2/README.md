# Segundo semestre (enero-mayo del 2018)

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63044    | T126  |  8  | Seminario | Filosofía de la cultura | Hegel y el pensamiento decolonial: crítica como progreso | Dr. Luis Guzmán |
|   10    |   63047    | T126  |  4  |   Curso   | Filosofía de la cultura | Lectura y comentario de La fenomenología del espíritu de Hegel | Dra. Virginia López-Domínguez |
|   10    |   63754    | T001  |  4  | Seminario | Maestría en Bibliotecología y Estudios de la Información | Tecnología de la información y sociedad | Dra. Georgina Araceli Torres Vargas |
